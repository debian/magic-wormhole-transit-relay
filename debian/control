Source: magic-wormhole-transit-relay
Section: python
Priority: optional
Maintainer: Antoine Beaupré <anarcat@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               python3-autobahn,
               python3-setuptools,
               python3-twisted (>= 17.5.0~),
Standards-Version: 4.6.1
Homepage: https://github.com/warner/magic-wormhole-transit-relay
Vcs-Git: https://salsa.debian.org/debian/magic-wormhole-transit-relay.git
Vcs-Browser: https://salsa.debian.org/debian/magic-wormhole-transit-relay

Package: magic-wormhole-transit-relay
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: Transit Relay server for Magic-Wormhole
 This repository implements the Magic-Wormhole "Transit Relay", a
 server that helps clients establish bulk-data transit connections
 even when both are behind NAT boxes. Each side makes a TCP connection
 to this server and presents a handshake. Two connections with
 identical handshakes are glued together, allowing them to pretend
 they have a direct connection.
 .
 This server used to be included in the magic-wormhole repository, but
 was split out into a separate repo to aid deployment and development.
